import os
import sys
import xml.etree.ElementTree as Et
from main_script import build_hsm
from main_script import build_hsm_triggers
from main_script import build_hsm_states
from main_script import build_hsm_fragment

# Name of the Android Studio package
states_class_name = "States"
triggers_class_name = "Triggers"


def open_xml(path):
    tree = Et.parse(path)
    return tree.getroot()


def build_fragment_state_machine_class():
    # Call build_hsm_fragment script to build the kotlin fragment HSM file
    build_hsm_fragment.write_fragment_hsm_file(hsm_name, hsm_data)


def build_state_machine_class():
    # Call build_hsm script to build the kotlin HSM file
    build_hsm.write_hsm_file((hsm_name + "Hsm"), package_name, hsm_data)


def build_states_sealed_class():
    # List with all the states
    states = []

    # Get a list of all states
    for datum in hsm_data:
        states.append(datum[0])

    # Call build_hsm_states script to build the States.kt file
    build_hsm_states.write_states_file(states_class_name, package_name, states)


def build_triggers_sealed_class():
    # List with all the triggers
    triggers = []

    # Get a list of all triggers
    for datum in hsm_data:
        for trigger in datum[3]:
            if len(trigger) > 0:
                triggers.append(trigger[0])

    # Call build_hsm_triggers script to build the Triggers.kt file
    build_hsm_triggers.write_triggers_file(triggers_class_name, package_name, triggers)


def get_state_machine_data():
    # Holds state machine information (State name, State entry methods, State exit methods, Transition data)
    # hsm_states =
    # [
    #    ['st_name_1', [st_ent_met_1, st_ent_met_2], [st_ext_met_1, st_ext_met_2], transition_list_1],
    #    ['st_name_2', [st_ent_met_1, st_ent_met_2], [st_ext_met_1, st_ext_met_2], transition_list_2]
    # ]
    # transition_list =
    # [
    #    ['trigger_name_1', 'trigger_guard_1', 'trigger_effect_1', 'trigger_destination_state_1'],
    #    ['trigger_name_2', 'trigger_guard_2', 'trigger_effect_2', 'trigger_destination_state_2']
    # ]
    hsm_states = list()
    # Holds temporary state machine information (State name, State entry methods, State exit methods)
    hsm_states_temp = list()
    # Holds temporary state machine transitions (Trigger name, Trigger guard, Trigger effect, Trigger destination)
    hsm_transitions_temp = list()

    # Get "element" from xml file
    # This "element" has the attribute xmi:type which indicates if the current "element" represents a State from the State Machine diagram
    for element in root.findall('{http://www.omg.org/spec/XMI/20131001}Extension/elements/element'):
        # Temporary variable that holds the entry methods for the current state
        state_entry_methods = []
        # Temporary variable that holds the exit methods for the current state
        state_exit_methods = []
        # Temporary variable that holds the name and the idref of the current state
        state_data = []

        # Check if the xmi:type == uml:State. This type represents a simple state in the State Machine diagram
        # If true, then get the name and the idref of the state
        if 'uml:State' == element.get('{http://www.omg.org/spec/XMI/20131001}type'):
            state_name = element.get('name')
            state_data.append(state_name)

            # Check if current state has entry/exit methods
            for states in element.findall('extendedProperties/states'):
                method_type = states.get('type')
                # Check if method_type is entry
                if 'entry' == method_type:
                    entry_method = states.get('name')
                    state_entry_methods.append(entry_method)
                # Check if method_type is exit
                if 'exit' == method_type:
                    exit_method = states.get('name')
                    state_exit_methods.append(exit_method)
            # Add a new element to the temporary state list with the information below
            hsm_states_temp.append([state_name, state_entry_methods, state_exit_methods])

    # Iterate through all the state machines to get their transitions to other states
    for state in hsm_states_temp:
        # Get state transition information (Trigger name, trigger guard, trigger effect, Trigger destination)
        for connector in root.findall('{http://www.omg.org/spec/XMI/20131001}Extension/connectors/connector'):
            # Get transition information from the state machine diagram
            ext_property = connector.find('extendedProperties')
            trigger_name = ext_property.get('privatedata1')
            trigger_guard = ext_property.get('privatedata2')
            trigger_effect = ext_property.get('privatedata3')
            if trigger_effect is not None:
                # Capitalize first letter of the trigger effect
                trigger_effect = trigger_effect[0].upper() + trigger_effect[1:]
            # Indicates the start of the trigger
            source_state = connector.find('source/model').get('name')
            # Indicates the end of the trigger
            destination_state = connector.find('target/model').get('name')

            # If transition goes from state[0](source_state) to destination_state, then get the transition data
            if state[0] == source_state:
                hsm_transitions_temp.append([trigger_name, trigger_guard, trigger_effect, destination_state])

        hsm_transitions_temp.sort()
        hsm_states.append([state[0], state[1], state[2], hsm_transitions_temp])
        # Clean temporary transition data
        hsm_transitions_temp = []

    hsm_states.append(['NoStateChange', [], [], [[]]])
    hsm_states.sort()

    return hsm_states


if __name__ == '__main__':
    if len(sys.argv) == 3:
        print(os.path.basename(__file__) + " is running!")

        xml_name = sys.argv[1]
        # Open State Machine xml
        # root = open_xml("../../.design/xml/" + xml_name + ".xml")
        root = open_xml("/home/victor/Documents/AndroidProjects/Phisio/design/xml/ScheduleAppointment.xml")
        # Get the name of the state machine from the package name
        hsm_name = ""
        for hsm_package in root.findall('{http://www.omg.org/spec/UML/20131001}Model/packagedElement'):
            hsm_name = hsm_package.get('name')

        # Open AndroidManifest.xml
        # android_root = open_xml("../../build/app/src/main/AndroidManifest.xml")
        android_root = open_xml("/home/victor/Documents/AndroidProjects/Phisio/build/app/src/main/AndroidManifest.xml")
        # Get the name of the Android package name
        package_name = android_root.get('package')

        hsm_data = get_state_machine_data()
        build_states_sealed_class()
        build_triggers_sealed_class()
        build_state_machine_class()
        build_fragment_state_machine_class()
    elif len(sys.argv) > 2:
        print("Too much arguments! Write only the name of the xml file.")
    else:
        print("Enter the name of the xml file!")
