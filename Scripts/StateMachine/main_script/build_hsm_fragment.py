import os
from write_class.build_class import WriteKtClassFile


def write_start_of_the_class(obj, class_name):
    obj.write_with_linesep("class " + class_name + "Fragment : " + class_name + "Hsm() {")
    obj.write_with_linesep_tab_indentation("/**----------------------------------------------------------------------------------------------------------------"
                                           "--------------------------------------------")
    obj.write_with_linesep_tab_indentation("* -----------------------------------------------------------------------------------------------------------------"
                                           "--------------------------------------------")
    obj.write_with_linesep_tab_indentation("* STARTS AUTO-GENERATED CODE")
    obj.write_with_linesep_tab_indentation("* -----------------------------------------------------------------------------------------------------------------"
                                           "--------------------------------------------")
    obj.write_with_linesep_tab_indentation("*------------------------------------------------------------------------------------------------------------------"
                                           "------------------------------------------*/")
    obj.write_with_linesep_tab_indentation("// Tag used for the debug logs")
    obj.write_with_linesep_tab_indentation("private val tag = this::class.simpleName")
    obj.write_with_linesep('')
    obj.write_with_linesep_tab_indentation("// Contains the current state of the " + class_name + "State Machine")
    obj.write_with_linesep_tab_indentation("private var currentState by Delegates.vetoable<States>(States.WaitUserData) { _, old, new ->")
    obj.write_with_linesep_double_tab_indentation("// Update UI state machine")
    obj.write_with_linesep_double_tab_indentation("if (new != States.NoStateChange) {")
    obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "renderViewState(old, new)")
    obj.write_with_linesep_double_tab_indentation("}")
    obj.write_with_linesep('')
    obj.write_with_linesep_double_tab_indentation("// Set currentState = new")
    obj.write_with_linesep_double_tab_indentation("new != States.NoStateChange")
    obj.write_with_linesep_tab_indentation("}")
    obj.write_with_linesep('')


def write_hsm_methods(obj, hsm_data, hsm_name):
    # ###################################################
    #  Starts writing the executeTriggerEffect function
    # ###################################################
    obj.write_with_linesep_tab_indentation("/**")
    obj.write_with_linesep_tab_indentation(" * This function executes the effect of the current trigger.")
    obj.write_with_linesep_tab_indentation(" *")
    obj.write_with_linesep_tab_indentation(" * @param effect It is the effect that must be executed for the trigger.")
    obj.write_with_linesep_tab_indentation(" */")
    obj.write_with_linesep_tab_indentation("private fun executeTriggerEffect(effect: TriggersEffect) {")
    obj.write_with_linesep_double_tab_indentation("when {")

    # Get a triggers effect list
    triggers_effect = []
    for hsm_datum in hsm_data:
        for transition_data in hsm_datum[3]:
            # Check length of transition_data
            if len(transition_data):
                if transition_data[2] is not None:
                    triggers_effect.append(transition_data[2])
    # Create a triggers effect list without repeated elements
    triggers_effect = obj.create_non_repeated_list(triggers_effect)

    for effect in triggers_effect:
        obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "TriggersEffect." + effect + " == effect -> {")
        obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + effect[0].lower() + effect[1:] + "()")
        obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "}")

    obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "TriggersEffect." + "NoTriggerEffect" + " == effect -> {")
    obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + "Log.d(tag, \"${this::executeTriggerEffect.name} >> No trigger effect\")")
    obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "}")

    # Close when expression
    obj.write_with_linesep_double_tab_indentation("}")
    # Close executeTriggerEffect function
    obj.write_with_linesep_tab_indentation("}")
    obj.write_with_linesep('')
    # ###################################################
    #  Ends writing the executeTriggerEffect function
    # ###################################################

    # ###################################################
    #  Start writing the renderViewState function
    # ###################################################
    obj.write_with_linesep_tab_indentation("/******************************************************************************************************************"
                                           "********************************************")
    obj.write_with_linesep_tab_indentation(" * " + hsm_name + " State Machine")
    obj.write_with_linesep_tab_indentation(" ******************************************************************************************************************"
                                           "********************************************/")
    obj.write_with_linesep_tab_indentation("/**")
    obj.write_with_linesep_tab_indentation(" * This function renders the UI state machine.")
    obj.write_with_linesep_tab_indentation(" *")
    obj.write_with_linesep_tab_indentation(" * @param oldState Represents the previous state of the " + hsm_name + " State Machine.")
    obj.write_with_linesep_tab_indentation(" * @param newState Represents the next state of the " + hsm_name + " State Machine.")
    obj.write_with_linesep_tab_indentation(" */")
    obj.write_with_linesep_tab_indentation("private fun renderViewState(oldState: States, newState: States) {")

    # Write the on exit functions and triggers effect for every state
    exit_fun_list = []
    entry_fun_list = []
    obj.write_with_linesep_double_tab_indentation("// On exit actions")
    obj.write_with_linesep_double_tab_indentation("when (oldState) {")
    for hsm_datum in hsm_data:
        state = hsm_datum[0]
        if state != "NoStateChange":
            exit_funs = hsm_datum[2]
            obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "is States." + state + " -> {")
            obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + "// Print debug logs")
            obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + "Log.d(tag, \">> ${this::renderViewState.name} "
                                                                                       "On exit: $oldState state\")")
            obj.write_with_linesep('')

            # Write on exit functions if any
            if len(exit_funs) > 0:
                obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + "// Execute on exit functions")
                for function in exit_funs:
                    exit_fun_list.append(function)
                    obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + function + "()")
                obj.write_with_linesep('')
            # Write trigger effect function
            obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + "// Execute trigger effect function")
            obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + "executeTriggerEffect(currentTriggerEffect)")
            obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "}")
    obj.write_with_linesep_double_tab_indentation("}")

    # Write the on entry functions for every state
    obj.write_with_linesep('')
    obj.write_with_linesep_double_tab_indentation("// On entry actions")
    obj.write_with_linesep_double_tab_indentation("when (newState) {")
    for hsm_datum in hsm_data:
        state = hsm_datum[0]
        if state != "NoStateChange":
            entry_funs = hsm_datum[1]
            obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "is States." + state + " -> {")
            obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + "// Print debug logs")
            obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + "Log.d(tag, \">> ${this::renderViewState.name} "
                                                                                       "On entry: $newState state\")")

            # Write on entry functions if any
            if len(entry_funs) > 0:
                obj.write_with_linesep('')
                obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + "// Execute on entry functions")
                for function in entry_funs:
                    entry_fun_list.append(function)
                    obj.write_with_linesep_double_tab_indentation(obj.double_tab_indentation + function + "()")
            obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "}")
    obj.write_with_linesep_double_tab_indentation("}")

    # Close renderViewState function
    obj.write_with_linesep_tab_indentation("}")
    # ###################################################
    #  Ends writing the renderViewState function
    # ###################################################

    # ###################################################
    #  Starts writing the updateStateMachine function
    # ###################################################
    obj.write_with_linesep('')
    obj.write_with_linesep_tab_indentation("/**")
    obj.write_with_linesep_tab_indentation(" * This function updates the ScheduleAppointment state machine.")
    obj.write_with_linesep_tab_indentation(" *")
    obj.write_with_linesep_tab_indentation(" * Use this function to feed the UI State Machine and be able to transition from one state to another.")
    obj.write_with_linesep_tab_indentation(" *")
    obj.write_with_linesep_tab_indentation(" * @param trigger That will feed the State Machine.")
    obj.write_with_linesep_tab_indentation(" */")
    obj.write_with_linesep_tab_indentation("private fun updateStateMachine(trigger: Triggers) {")
    obj.write_with_linesep_double_tab_indentation("currentState = consumeTrigger(trigger, currentState)")
    obj.write_with_linesep_tab_indentation("}")
    # ###################################################
    #  Ends writing the updateStateMachine function
    # ###################################################

    # ###################################################
    #  Starts writing the on entry functions template
    # ###################################################
    obj.write_with_linesep('')
    obj.write_with_linesep_tab_indentation("/******************************************************************************************************************"
                                           "********************************************")
    obj.write_with_linesep_tab_indentation(" * On entry functions")
    obj.write_with_linesep_tab_indentation(" ******************************************************************************************************************"
                                           "********************************************/")
    for fun in entry_fun_list:
        obj.write_fun_template(fun, "private", None)
        obj.write_with_linesep('')
    # ###################################################
    #  Starts writing the on entry functions template
    # ###################################################

    # ###################################################
    #  Starts writing the on exit functions template
    # ###################################################
    obj.write_with_linesep_tab_indentation("/******************************************************************************************************************"
                                           "********************************************")
    obj.write_with_linesep_tab_indentation(" * On exit functions")
    obj.write_with_linesep_tab_indentation(" ******************************************************************************************************************"
                                           "********************************************/")
    for fun in exit_fun_list:
        obj.write_fun_template(fun, "private", None)
        obj.write_with_linesep('')
    # ###################################################
    #  Ends writing the on exit functions template
    # ###################################################

    # ###################################################
    #  Starts writing the trigger effect functions template
    # ###################################################
    obj.write_with_linesep_tab_indentation("/******************************************************************************************************************"
                                           "********************************************")
    obj.write_with_linesep_tab_indentation(" * Trigger effect functions")
    obj.write_with_linesep_tab_indentation(" ******************************************************************************************************************"
                                           "********************************************/")
    for effect in triggers_effect:
        obj.write_fun_template((effect[0].lower() + effect[1:]), "private", None)
        obj.write_with_linesep('')
    # ###################################################
    #  Ends writing the on trigger effect function templates
    # ###################################################

    # ###################################################
    #  Starts writing the guard functions template
    # ###################################################
    obj.write_with_linesep_tab_indentation("/******************************************************************************************************************"
                                           "********************************************")
    obj.write_with_linesep_tab_indentation(" * Guard functions")
    obj.write_with_linesep_tab_indentation(" ******************************************************************************************************************"
                                           "********************************************/")

    # Create a guard list with non repeated elements
    guard_list = []
    for hsm_datum in hsm_data:
        for transition_data in hsm_datum[3]:
            # Check length of transition_data
            if len(transition_data):
                if transition_data[1] is not None:
                    guard = transition_data[1]
                    guard_list.append(guard.replace('!',''))
    guard_list = obj.create_non_repeated_list(guard_list)

    # Write guard functions
    for guard in guard_list:
        obj.write_fun_template(guard, "override", "Boolean")
        obj.write_with_linesep('')

    # ###################################################
    #  Ends writing the on trigger effect function templates
    # ###################################################

    obj.write_with_linesep_tab_indentation("/**----------------------------------------------------------------------------------------------------------------"
                                           "--------------------------------------------")
    obj.write_with_linesep_tab_indentation("* -----------------------------------------------------------------------------------------------------------------"
                                           "--------------------------------------------")
    obj.write_with_linesep_tab_indentation("* ENDS AUTO-GENERATED CODE")
    obj.write_with_linesep_tab_indentation("* -----------------------------------------------------------------------------------------------------------------"
                                           "--------------------------------------------")
    obj.write_with_linesep_tab_indentation("*------------------------------------------------------------------------------------------------------------------"
                                           "------------------------------------------*/")
    obj.write_file("}")


def write_fragment_hsm_file(hsm_name, hsm_data):
    print("\n" + os.path.basename(__file__) + " is running!")

    # Instantiate object
    write_hsm = WriteKtClassFile((hsm_name + "Fragment"))

    write_start_of_the_class(write_hsm, hsm_name)
    write_hsm_methods(write_hsm, hsm_data, hsm_name)


if __name__ == '__main__':
    write_fragment_hsm_file('')