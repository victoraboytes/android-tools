import write_class.build_sealed_class as build_sealed_class
import os


# This function writes the states sealed class.
#
# @param class_name It is the sealed class name.
# @param package_name It is the package name of the Android project.
# @param states It is the list that contains all the states of the state machine.
#               states = ['state_name_1', 'state_name_2', 'state_name_3', ...]
def write_states_file(class_name, package_name, states):
    print("\n" + os.path.basename(__file__) + " is running!")

    # Instantiate WriteSealedClassFile
    sealed_class = build_sealed_class.WriteSealedClassFile(class_name, (package_name + ".presentation.statemachine"), states)
    # Write sealed class
    sealed_class.write_sealed_class()


if __name__ == '__main__':
    write_states_file("", "", ["", ""])