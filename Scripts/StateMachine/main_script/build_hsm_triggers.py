import write_class.build_sealed_class as build_sealed_class
import os


# This function writes the triggers sealed class.
#
# @param class_name   It is the sealed class name.
# @param package_name It is the package name of the Android project.
# @param triggers     It is the list that contains all the triggers of the state machine.
#                     triggers = ['trigger_name_1', 'trigger_name_2', 'trigger_name_3', ...]
def write_triggers_file(class_name, package_name, triggers):
    print("\n" + os.path.basename(__file__) + " is running!")

    # Instantiate WriteSealedClassFile
    sealed_class = build_sealed_class.WriteSealedClassFile(class_name, (package_name + ".presentation.statemachine"), triggers)
    # Write sealed class
    sealed_class.write_sealed_class()


if __name__ == '__main__':
    write_triggers_file("", "", ["", ""])