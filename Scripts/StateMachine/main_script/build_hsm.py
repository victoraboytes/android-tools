import os
from write_class.build_class import WriteKtClassFile
from main_script.main import states_class_name
from main_script.main import triggers_class_name


# This function writes the trigger guards abstract methods.
#
# @param obj      Instance of the WriteKtClassFile class.
# @param triggers Contains a list with all the triggers of the State Machine.
#                 triggers = ['trigger_guard_1', 'trigger_guard_2', 'trigger_guard_3', ...]
def write_trigger_guards_method(obj, guards):
    # Create a list with non-repeated guards
    non_repeated_triggers_list = obj.create_non_repeated_list(guards)

    # Write the guards methods declaration
    obj.write_with_linesep('')
    obj.write_with_linesep(obj.tab_indentation + "// State machine guards")
    for guard in non_repeated_triggers_list:
        obj.write_with_linesep(obj.tab_indentation + "protected abstract fun " + guard + "(): Boolean")


# This function writes the trigger effects enum class.
#
# @param obj      Instance of the WriteKtClassFile class.
# @param triggers Contains a list with all the triggers of the State Machine.
#                 triggers = ['trigger_effect_1', 'trigger_effect_2', 'trigger_effect_3', ...]
def write_triggers_effect_ids(obj, triggers):
    # Create a list with non-repeated triggers
    non_repeated_triggers_list = obj.create_non_repeated_list(triggers)

    # Name of the enum class
    enum_class_name = "TriggersEffect"

    # Write nested enum class
    obj.write_nested_enum_class(enum_class_name, "protected", non_repeated_triggers_list, "// Trigger effect IDs")

    # Write currentTriggerEffect variable
    obj.write_with_linesep('')
    obj.write_with_linesep_tab_indentation("// Current trigger effect")
    obj.write_with_linesep_tab_indentation("protected var currentTriggerEffect = " + enum_class_name + ".NoTriggerEffect")


def write_consume_trigger_body_method(obj, hsm_data):
    state_name_idx = 0
    trigger_name_idx = 0
    transition_list_idx = 3
    trigger_guard_idx = 1
    trigger_effect_idx = 2
    trigger_dest_idx = 3

    # Open outer when expression to evaluate the current state
    obj.write_with_linesep_double_tab_indentation("// Check for current state")
    obj.write_with_linesep_double_tab_indentation("when (currentState) {")

    # Write the state branches for the when expression to evaluate the current state
    # Iterate through all states of the hsm_data
    for hsm_datum in hsm_data:
        state_name = hsm_datum[state_name_idx]

        # Open state branch
        obj.write_with_linesep_triple_tab_indentation("is " + states_class_name + '.' + state_name + " -> {")
        # Write special branch statements if state_name is "NoStateChange"
        if state_name is not "NoStateChange":
            # Open inner when expression to evaluate the current trigger
            obj.write_with_linesep_four_tab_indentation("// Check for current trigger")
            obj.write_with_linesep_four_tab_indentation("when (currentTrigger) {")

            # Get a list with all the transitions that belongs to current state (state_name)
            # The list does not contain repeatable transitions
            state_transition_list = []
            for transition_data in hsm_datum[transition_list_idx]:
                state_transition_list.append(transition_data[trigger_name_idx])
            state_transition_list = obj.create_non_repeated_list(state_transition_list)

            # Write the statements of every trigger branch for the current state (state_name)
            for trigger_name in state_transition_list:
                # Flag that indicates if the "if guard statement" was written in trigger branch from inner when expression
                num_of_guards_written = 0
                guard_name_written = ""
                # Open trigger branch of the inner when expression
                obj.write_with_linesep_five_tab_indentation("is " + triggers_class_name + '.' + trigger_name + " -> {")
                # Find the transition data of the trigger_name from hsm_datum[transition_list_idx]
                # Write all the guards conditions from the current state's trigger
                for transition_data in hsm_datum[transition_list_idx]:
                    if trigger_name == transition_data[trigger_name_idx]:
                        trigger_guard = transition_data[trigger_guard_idx]
                        trigger_dest = transition_data[trigger_dest_idx]
                        trigger_effect = transition_data[trigger_effect_idx]

                        # Write guard condition if any. If not, then write trigger branch statements without if statement for guard conditions
                        if trigger_guard is not None:
                            num_of_guards_written += 1
                            # Save name of the guard
                            if num_of_guards_written == 1:
                                guard_name_written = trigger_guard
                                # Open if statement with guard condition
                                obj.write_with_linesep_six_tab_indentation("if (" + trigger_guard + "()) {")
                            if num_of_guards_written == 2:
                                # Open else statement for the first if statement with guard condition
                                obj.write_with_linesep_six_tab_indentation("else {")
                            obj.write_with_linesep_seven_tab_indentation("nextState = " + states_class_name + '.' + trigger_dest)
                            obj.write_with_linesep('')
                            obj.write_with_linesep_seven_tab_indentation("// Print debug logs")
                            obj.write_with_linesep_seven_tab_indentation("printStateTransition(currentState, nextState, currentTrigger)")
                            # Write trigger effect if any
                            if trigger_effect is not None:
                                obj.write_with_linesep('')
                                obj.write_with_linesep_seven_tab_indentation("// Call trigger effect")
                                obj.write_with_linesep_seven_tab_indentation("currentTriggerEffect = TriggersEffect." + trigger_effect)
                            # Close if statement with guard condition
                            obj.write_with_linesep_six_tab_indentation("}")
                        else:
                            obj.write_with_linesep_six_tab_indentation("nextState = " + states_class_name + '.' + trigger_dest)
                            obj.write_with_linesep('')
                            obj.write_with_linesep_six_tab_indentation("// Print debug logs")
                            obj.write_with_linesep_six_tab_indentation("printStateTransition(currentState, nextState, currentTrigger)")
                            # Write trigger effect if any
                            if trigger_effect is not None:
                                obj.write_with_linesep('')
                                obj.write_with_linesep_six_tab_indentation("// Call trigger effect")
                                obj.write_with_linesep_six_tab_indentation("currentTriggerEffect = TriggersEffect." + trigger_effect)
                # Write else statement if only 1 if guard statement was written inside trigger branch from inner when expression
                if num_of_guards_written == 1:
                    obj.write_with_linesep_six_tab_indentation("else {")
                    obj.write_with_linesep_seven_tab_indentation("// Print debug logs")
                    obj.write_with_linesep_seven_tab_indentation("printInvalidGuard(currentState, currentTrigger, \"" + guard_name_written + "\")")
                    obj.write_with_linesep_six_tab_indentation("}")
                # Close trigger branch of the inner when expression
                obj.write_with_linesep_five_tab_indentation("}")

            # Write else branch for inner when expression to evaluate the current trigger
            obj.write_with_linesep_five_tab_indentation("else -> {")
            obj.write_with_linesep_six_tab_indentation("// Print debug logs")
            obj.write_with_linesep_six_tab_indentation("printInvalidTrigger(currentState, currentTrigger)")
            obj.write_with_linesep_five_tab_indentation("}")

            # Close inner when expression to evaluate the current trigger
            obj.write_with_linesep_four_tab_indentation("}")

            # Close state branch of the outer when expression
            obj.write_with_linesep_triple_tab_indentation("}")
        # Write is States.NoStateChange -> { statements
        else:
            obj.write_with_linesep_four_tab_indentation("nextState = " + states_class_name + '.' + state_name)
            obj.write_with_linesep('')
            obj.write_with_linesep_four_tab_indentation("Log.d(tag, \"${this::consumeTrigger.name} >> No state change\")")
            # Close is States.NoStateChange branch
            obj.write_with_linesep_triple_tab_indentation("}")

    # Close outer when expression to evaluate the current state
    obj.write_with_linesep_double_tab_indentation("}")

    # Write return statement
    obj.write_with_linesep('')
    obj.write_with_linesep_double_tab_indentation("return nextState")


def write_consume_trigger_method(obj, hsm_data):
    # Open consumeTrigger fun
    obj.write_with_linesep('')
    obj.write_with_linesep_tab_indentation("protected fun consumeTrigger(currentTrigger: Triggers, currentState: States): States {")
    obj.write_with_linesep_double_tab_indentation("var nextState: States = States.NoStateChange")
    obj.write_with_linesep_double_tab_indentation("currentTriggerEffect = TriggersEffect.NoTriggerEffect")
    obj.write_with_linesep('')

    # Write UI state machine logic
    write_consume_trigger_body_method(obj, hsm_data)

    # Close consumeTrigger fun
    obj.write_with_linesep_tab_indentation("}")


def write_print_functions(obj):
    # Write printStateTransition fun
    obj.write_with_linesep('')
    obj.write_with_linesep_tab_indentation("private fun printStateTransition(currentState: States, nextState: States, currentTrigger: Triggers) {")
    obj.write_with_linesep_double_tab_indentation("Log.d(tag, \"${this::consumeTrigger.name} >> Transition from $currentState state to $nextState state by "
                                                  "$currentTrigger trigger\")")
    obj.write_with_linesep_tab_indentation("}")
    obj.write_with_linesep('')

    # Write printStateTransition fun
    obj.write_with_linesep_tab_indentation("private fun printInvalidTrigger(currentState: States, currentTrigger: Triggers) {")
    obj.write_with_linesep_double_tab_indentation("Log.d(tag, \"${this::consumeTrigger.name} >> $currentTrigger trigger is invalid for $currentState "
                                                  "state\")")
    obj.write_with_linesep_tab_indentation("}")
    obj.write_with_linesep('')

    # Write printStateTransition fun
    obj.write_with_linesep_tab_indentation("private fun printInvalidGuard(currentState: States, currentTrigger: Triggers, guard: String) {")
    obj.write_with_linesep_double_tab_indentation("if (guard.first() == '!') {")
    obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "Log.d(tag, \"${this::consumeTrigger.name} >> Invalid guard condition! "
                                                                        "${guard.removePrefix(\"!\")} = True for $currentTrigger trigger in "
                                                                        "$currentState state\")")
    obj.write_with_linesep_double_tab_indentation("} else {")
    obj.write_with_linesep_double_tab_indentation(obj.tab_indentation + "Log.d(tag, \"${this::consumeTrigger.name} >> Invalid guard condition! "
                                                                        "${guard.removePrefix(\"!\")} = False for $currentTrigger trigger in "
                                                                        "$currentState state\")")
    obj.write_with_linesep_double_tab_indentation("}")
    obj.write_with_linesep_tab_indentation("}")


# This function writes the HSM class.
#
# @param class_name   It is the sealed class name.
# @param package_name It is the package name of the Android project.
# @param hsm_data     Holds state machine information (State name, State entry methods, State exit methods, Transition data)
#                     hsm_data =
#                     [
#                        ['st_name_1', [st_ent_met_1, st_ent_met_2], [st_ext_met_1, st_ext_met_2], transition_list_1],
#                        ['st_name_2', [st_ent_met_1, st_ent_met_2], [st_ext_met_1, st_ext_met_2], transition_list_2]
#                     ]
#                     transition_list =
#                     [
#                        ['trigger_name_1', 'trigger_guard_1', 'trigger_effect_1', 'trigger_destination_state_1'],
#                        ['trigger_name_2', 'trigger_guard_2', 'trigger_effect_2', 'trigger_destination_state_2']
#                     ]
def write_hsm_file(class_name, package_name, hsm_data):
    print("\n" + os.path.basename(__file__) + " is running!")

    # Instantiate object
    write_hsm = WriteKtClassFile(class_name)

    # Write package
    write_hsm.write_with_double_linesep("package " + package_name + ".presentation.statemachine")

    # Open class
    write_hsm.write_with_linesep("import android.util.Log")
    write_hsm.write_with_double_linesep("import androidx.appcompat.app.AppCompatActivity")
    write_hsm.write_with_linesep("abstract class " + class_name + ": AppCompatActivity() {")
    write_hsm.write_with_linesep(write_hsm.tab_indentation + "// Tag for debug logs")
    write_hsm.write_with_double_linesep(write_hsm.tab_indentation + "private val tag = this::class.simpleName")

    # Trigger effect list
    triggers_effect = []
    # Guard list
    triggers_guard = []
    # Get a triggers guard and effect list
    for hsm_datum in hsm_data:
        for transition_data in hsm_datum[3]:
            if len(transition_data) > 0:
                if transition_data[2] is not None:
                    triggers_effect.append(transition_data[2])
                if transition_data[1] is not None:
                    # Remove '!' character from guard. A guard can be either guard_1 or !guard_1
                    triggers_guard.append(transition_data[1].replace('!', ''))

    # Add NoTriggerEffect to triggers effect list
    triggers_effect.append("NoTriggerEffect")
    triggers_guard.sort()
    triggers_effect.sort()

    # Write triggers effect enum class
    write_triggers_effect_ids(write_hsm, triggers_effect)

    # Write triggers guard methods
    write_trigger_guards_method(write_hsm, triggers_guard)

    # Write consumeTrigger method
    write_consume_trigger_method(write_hsm, hsm_data)

    write_print_functions(write_hsm)

    # Close class
    write_hsm.write_file("}")


if __name__ == '__main__':
    write_hsm_file("", "", ["", ""])
