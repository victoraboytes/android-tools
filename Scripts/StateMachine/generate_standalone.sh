
echo "================================"
echo "Generating state machine binary!"
echo "================================"
echo ""

# Add $HOME/.local/bin to PATH in order to run pyinstaller command
export PATH="$HOME/.local/bin:$PATH"

# Generate standalone application
pyinstaller --onefile main_script/main.py

# Move python standalone application to specified directory
mv "dist/main" $1

# Remove build and dist directory and main.spec file
wait
rm -r build
rm -r dist
rm -r main.spec

