from write_class.write_file import WriteFile


class WriteSealedClassFile(WriteFile):

    # Constructor for WriteSealedClassFile.
    #
    # @param document_name Contains the name with the directory path of the file to be created. i.e. /home/victor/Documents/{document_name}
    # @param package_name  Contains the package name of the android project
    # @param data          It is the list that contains the data objects to be written to the file<.
    #                      data = ['data_name_1', 'data_name_2', 'data_name_3']
    def __init__(self, class_name, package_name, data):
        super().__init__((class_name + ".kt"))

        self.class_name = class_name
        self.package_name = package_name
        self.data = data

    # This function generates the sealed class
    def write_sealed_class(self):
        self.__write_header()
        self.__write_body()
        self.__write_last_part()

    def __write_header(self):
        self.write_with_double_linesep("package " + self.package_name)
        self.write_with_linesep("sealed class " + self.class_name + " {")

    def __write_body(self):
        # Create a list with non repeated elements
        non_repeated_element_list = self.create_non_repeated_list(self.data)
        # Write all the objects for the sealed class
        for element in non_repeated_element_list:
            self.write_file(self.tab_indentation + "object " + element + ": " + self.class_name + "()")
            self.write_file(self.line_sep)

    def __write_last_part(self):
        # Write toString() function and close file
        self.write_file(self.line_sep)
        self.write_with_linesep(self.tab_indentation + "override fun toString(): String {")
        self.write_with_linesep(self.double_tab_indentation + "return \"${this::class.simpleName}\"")
        self.write_with_linesep(self.tab_indentation + "}")
        self.write_file("}")

        # Close file
        self.close_file()