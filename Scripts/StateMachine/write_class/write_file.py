import os
from os import path


class WriteFile:

    def __init__(self, document_name):
        # Contains the reference of the file
        self.document = document_name
        self.line_sep = os.linesep
        self.tab_indentation = "    "
        self.double_tab_indentation = self.tab_indentation + self.tab_indentation

        # Create file
        self.__create_file(document_name)

    # This function creates a file with the name contained in document_name.
    #
    # @param document_name It's the name with the directory path of the file to be created. i.e. /home/victor/Documents/{document_name}
    def __create_file(self, document_name):
        if path.exists(document_name) is True:
            print("Removing " + document_name + " file!")
            os.remove(document_name)
            print("Creating " + document_name + " file!")
            self.document = open(document_name, "x")
        else:
            print("Creating " + document_name + " file!")
            self.document = open(document_name, "x")

    # This function appends a text to the file if self.document is not null.
    #
    # @param txt Contains the text to be written to the file.
    def write_file(self, txt):
        if self.document is not None:
            self.document.write(txt)

    # This function closes the file if self.document is not null.
    def close_file(self):
        if self.document is not None:
            self.document.close()

    # This function writes the txt followed by a double line separator.
    # For example:
    # "Some text"
    # {line separator}
    #
    # @param txt Contains the text to be written into the file.
    def write_with_double_linesep(self, txt):
        self.document.write(txt)
        self.document.write(os.linesep + os.linesep)

    # This function writes the txt followed by a line separator.
    # For example:
    # "Some text"
    #
    # @param txt Contains the text to be written into the file.
    def write_with_linesep(self, txt):
        self.document.write(txt)
        self.document.write(os.linesep)

    # This function writes the txt with tab indentation followed by a line separator.
    # For example:
    # [tab]"Some text"
    # {line separator}
    #
    # @param txt Contains the text to be written into the file.
    def write_with_linesep_tab_indentation(self, txt):
        self.document.write(self.tab_indentation + txt)
        self.document.write(os.linesep)

    # This function writes the txt with double tab indentation followed by a line separator.
    # For example:
    # [tab][tab]"Some text"
    # {line separator}
    #
    # @param txt Contains the text to be written into the file.
    def write_with_linesep_double_tab_indentation(self, txt):
        self.document.write(self.double_tab_indentation + txt)
        self.document.write(os.linesep)

    # This function writes the txt with triple tab indentation followed by a line separator.
    # For example:
    # [tab][tab][tab]"Some text"
    # {line separator}
    #
    # @param txt Contains the text to be written into the file.
    def write_with_linesep_triple_tab_indentation(self, txt):
        self.document.write(self.double_tab_indentation + self.tab_indentation + txt)
        self.document.write(os.linesep)

    # This function writes the txt with four tab indentation followed by a line separator.
    # For example:
    # [tab][tab][tab][tab]"Some text"
    # {line separator}
    #
    # @param txt Contains the text to be written into the file.
    def write_with_linesep_four_tab_indentation(self, txt):
        self.document.write(self.double_tab_indentation + self.double_tab_indentation + txt)
        self.document.write(os.linesep)

    # This function writes the txt with four tab indentation followed by a line separator.
    # For example:
    # [tab][tab][tab][tab][tab]"Some text"
    # {line separator}
    #
    # @param txt Contains the text to be written into the file.
    def write_with_linesep_five_tab_indentation(self, txt):
        self.document.write(self.double_tab_indentation + self.double_tab_indentation + self.tab_indentation + txt)
        self.document.write(os.linesep)

    # This function writes the txt with triple tab indentation followed.
    # For example:
    # [tab][tab][tab][tab][tab][tab]"Some text"
    #
    # @param txt Contains the text to be written into the file.
    def write_with_six_tab_indentation(self, txt):
        self.document.write(self.double_tab_indentation + self.double_tab_indentation + self.double_tab_indentation + txt)

    # This function writes the txt with triple tab indentation followed by a line separator.
    # For example:
    # [tab][tab][tab][tab][tab][tab]"Some text"
    # {line separator}
    #
    # @param txt Contains the text to be written into the file.
    def write_with_linesep_six_tab_indentation(self, txt):
        self.document.write(self.double_tab_indentation + self.double_tab_indentation + self.double_tab_indentation + txt)
        self.document.write(os.linesep)

    # This function writes the txt with triple tab indentation followed by a line separator.
    # For example:
    # [tab][tab][tab][tab][tab][tab][tab]"Some text"
    # {line separator}
    #
    # @param txt Contains the text to be written into the file.
    def write_with_linesep_seven_tab_indentation(self, txt):
        self.document.write(self.double_tab_indentation + self.double_tab_indentation + self.double_tab_indentation + self.tab_indentation + txt)
        self.document.write(os.linesep)

    # This function creates a list with non-repeated elements.
    #
    # The list return will have elements that are not None.
    #
    # @param list_of_elements Contains the base list that will be used to create the non-repeated element list.
    # @return A list with non-repeated elements. All the elements of the list are different to None type.
    def create_non_repeated_list(self, list_of_elements):
        # Holds the elements of the list_of_elements. This list does not contain repeated elements.
        non_repeated_list = []

        for idx, element in enumerate(list_of_elements):
            # Check if element is not None
            if element is not None:
                # Check if element is not repeated in non_repeated_list
                if self.element_is_duplicated(non_repeated_list, element) is False:
                    # Assign new effect to temp_effects list
                    non_repeated_list.append(element)

        return non_repeated_list

    # This function returns false if the element is not duplicated within the list_of_elements
    def element_is_duplicated(self, list_of_elements, element):
        duplicated = False

        for element_list in list_of_elements:
            if element_list == element:
                duplicated = True

        return duplicated
