from write_class.write_file import WriteFile


class WriteKtClassFile(WriteFile):
    def __init__(self, class_name):
        super().__init__(class_name + ".kt")

    # This function writes a function with no return type nor arguments. The output is the following:
    # /**
    #  * TODO("Add here the function description")
    #  */
    #  private fun function_name() {
    #      Log.d(tag, ">> ${this::function_name.name} function call")
    #      // TODO("Not yet implemented")
    #  }
    # IMPORTANT: This function is supposed to be written with 1 level of indentation. It means that the written
    # function is supposed to be inside 1 nested level.
    def write_fun_template(self, fun_name, modifier, output):
        self.write_with_linesep_tab_indentation("/**")
        self.write_with_linesep_tab_indentation(" * TODO(\"Add the function description\")")
        self.write_with_linesep_tab_indentation(" */")
        if output is None:
            self.write_with_linesep_tab_indentation(modifier + " fun " + fun_name + "() {")
        else:
            self.write_with_linesep_tab_indentation(modifier + " fun " + fun_name + "(): " + output + " {")
        self.write_with_linesep_double_tab_indentation("Log.d(tag, \">> ${this::" + fun_name + ".name} function call\")")
        self.write_with_linesep_double_tab_indentation("TODO(\"Not yet implemented\")")
        self.write_with_linesep_tab_indentation("}")

    def write_nested_enum_class(self, enum_class_name, visibility_mod, enum_elements, class_comment):
        self.write_with_linesep(self.tab_indentation + class_comment)
        self.write_with_linesep(self.tab_indentation + visibility_mod + " enum class " + enum_class_name + " {")

        # Return a list with non repeated enum elements
        enum_elements_non_repeated = self.create_non_repeated_list(enum_elements)

        # Write trigger effects elements
        for idx, effect in enumerate(enum_elements_non_repeated):
            # If effect is not the last enum element, then write the comma separator at the end of the enum element
            if (idx + 1) < len(enum_elements_non_repeated):
                self.write_with_linesep(self.double_tab_indentation + effect + ",")
            # If effect is the last enum element, then write only the enum element without the comma separator
            else:
                self.write_with_linesep(self.double_tab_indentation + effect)

        self.write_with_linesep(self.tab_indentation + "}")